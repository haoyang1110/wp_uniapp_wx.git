module.exports = {

    // 解决 vue 文件解析错误的问题
    chainWebpack: (config) => {
        config.module
            .rule('vue')
            .use('vue-loader')
            .loader('vue-loader')
            .tap((options) => {
                // 确保 Vue 解析器支持最新的 JavaScript 特性
                options.compilerOptions = {
                    // 启用 Vue 3 的模板编译
                    ...options.compilerOptions,
                    whitespace: 'condense',
                };
                return options;
            });
    },
    configureWebpack: {
        // 对构建过程中的 JavaScript 进行额外配置
        resolve: {
            alias: {
                // 确保 Vue 版本的别名正确，避免版本冲突
                vue$: 'vue/dist/vue.runtime.esm.js',
            },
        },
    },
    // 开发模式下的代理配置，避免跨域问题
    devServer: {
        proxy: 'http://localhost:8080', // 或者你的后端服务地址
    },
    css: {
        loaderOptions: {
            sass: {
                implementation: require('sass')
            }
        }
    }
} 