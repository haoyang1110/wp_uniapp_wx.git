const BASE_URL = 'https://lzphy.top'

export default {
  // 获取文章列表
  getArticles(page = 1, perPage = 10) {
    return new Promise((resolve, reject) => {
      uni.request({
        url: `${BASE_URL}/wp-json/wp/v2/posts`,
        data: {
          page,
          per_page: perPage,
          _fields: 'id,title,excerpt,date,content,categories,modified'
        },
        success: (res) => {
          // console.log('Response Headers:', res.header)
          // console.log('Articles Data:', res.data)

          if (res.statusCode === 200) {
            resolve({
              statusCode: 200,
              data: res.data
            })
          } else {
            resolve({
              statusCode: res.statusCode,
              data: []
            })
          }
        },
        fail: (err) => {
          console.error('API Error:', err)
          resolve({
            statusCode: 500,
            data: []
          })
        }
      })
    })
  },

  // 搜索文章
  searchArticles(keyword, page = 1) {
    return new Promise((resolve, reject) => {
      uni.request({
        url: `${BASE_URL}/wp-json/wp/v2/posts`,
        data: {
          search: keyword,
          page,
          per_page: 10,
          _fields: 'id,title,excerpt,date,content,categories,modified'
        },
        success: (res) => {
          if (res.statusCode === 200) {
            resolve({
              statusCode: 200,
              data: res.data
            })
          } else {
            resolve({
              statusCode: res.statusCode,
              data: []
            })
          }
        },
        fail: (err) => {
          console.error('Search API Error:', err)
          resolve({
            statusCode: 500,
            data: []
          })
        }
      })
    })
  },

  // 获取文章详情
  getArticleDetail(id) {
    return new Promise((resolve, reject) => {
      uni.request({
        url: `${BASE_URL}/wp-json/wp/v2/posts/${id}`,
        success: (res) => {
          if (res.statusCode === 200) {
            resolve({
              statusCode: 200,
              data: res.data
            })
          } else {
            resolve({
              statusCode: res.statusCode,
              data: null
            })
          }
        },
        fail: () => {
          resolve({
            statusCode: 500,
            data: null
          })
        }
      })
    })
  },

  // 获取文章的图片
  getPostImage(post) {
    try {
      // 从内容中提取第一张图片
      if (post.content && post.content.rendered) {
        const imgReg = /<img[^>]*src="([^"]*)"[^>]*>/
        const match = post.content.rendered.match(imgReg)
        if (match && match[1]) {
          return match[1]
        }
      }
      return 'https://lzphy.top/wp-content/uploads/2024/11/wp_editor_md_ab8de7ba3b3b6cd27c2957f95aab644a.jpg'
    } catch (e) {
      return 'https://lzphy.top/wp-content/uploads/2024/11/wp_editor_md_ab8de7ba3b3b6cd27c2957f95aab644a.jpg'
    }
  },

  // 获取分类列表
  getCategories() {
    return uni.request({
      url: `${BASE_URL}/wp-json/wp/v2/categories`,
      data: {
        _fields: 'id,name,count',
        per_page: 100,
        orderby: 'count',
        order: 'desc'
      },
      method: 'GET'
    })
  },

  // 添加获取分类详情的方法
  getCategoryDetail(id) {
    return uni.request({
      url: `${BASE_URL}/wp-json/wp/v2/categories/${id}`,
      method: 'GET'
    })
  },

  // 添加获取分类下的文章列表方法
  getCategoryPosts(categoryId, page = 1, perPage = 10) {
    return new Promise((resolve, reject) => {
      uni.request({
        url: `${BASE_URL}/wp-json/wp/v2/posts`,
        data: {
          categories: categoryId,
          page,
          per_page: perPage,
          _fields: 'id,title,excerpt,date,content,categories,modified'
        },
        success: (res) => {
          if (res.statusCode === 200) {
            resolve({
              statusCode: 200,
              data: res.data
            })
          } else {
            resolve({
              statusCode: res.statusCode,
              data: []
            })
          }
        },
        fail: (err) => {
          console.error('API Error:', err)
          resolve({
            statusCode: 500,
            data: []
          })
        }
      })
    })
  },

  // 添加获取标签列表的方法
  getTags() {
    return uni.request({
      url: `${BASE_URL}/wp-json/wp/v2/tags`,
      data: {
        _fields: 'id,name,count',
        per_page: 100,
        orderby: 'count',
        order: 'desc'
      },
      method: 'GET'
    })
  },

  // 获取标签详情
  getTagDetail(id) {
    return uni.request({
      url: `${BASE_URL}/wp-json/wp/v2/tags/${id}`,
      method: 'GET'
    })
  },

  // 修改获取标签下的文章列表方法
  // 修改获取标签下的文章列表方法
  getTagPosts(tagId, page = 1, perPage = 10) {
    // console.log('Calling getTagPosts with tagId:', tagId);
    return new Promise((resolve, reject) => {
      const url = `${BASE_URL}/wp-json/wp/v2/posts?tags=${tagId}&page=${page}&per_page=${perPage}&_fields=id,title,excerpt,date,content,categories,modified`;
      // console.log('Request URL:', url);  // 打印请求的 URL 以便调试

      uni.request({
        url: url,  // 使用构建好的 URL
        method: 'GET',
        success: (res) => {
          // console.log('Tag Posts Response:', res);
          if (res.statusCode === 200) {
            resolve({
              statusCode: 200,
              data: res.data
            });
          } else {
            resolve({
              statusCode: res.statusCode,
              data: []
            });
          }
        },
        fail: (err) => {
          console.error('Tag Posts API Error:', err);
          resolve({
            statusCode: 500,
            data: []
          });
        }
      });
    });
  }

} 