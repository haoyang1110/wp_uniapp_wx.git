# my-project

## 运行项目
```
npm install

yarn install
```

### Compiles and hot-reloads for development
```
yarn serve

```

### Compiles and minifies for production
```
yarn build
```

### 页面效果

![首页](src/static/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20241116164016.png)
![分类](src/static/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20241116164032.png)
![标签](src/static/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20241116164043.png)
![类别文章](src/static/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20241116164106.png)
![文章详情](src/static/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20241116184404.png)